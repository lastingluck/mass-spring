# README #

Run with the make command to compile the examples (run, run2, run3). Needs the Eigen library and the GSL library.

Write-up [here](https://sites.google.com/a/umn.edu/alexdahlgraphics/projects/mass-spring-cloth)

Currently working on improving this using the paper ["Robust Treatment of Collisions, Contact and Friction for Cloth Animation" (2002)](http://dl.acm.org/citation.cfm?id=566623). I will be subsituting some parts for more recent work. Section 4 will be replaced with ["Efficient Simulation of Inextensible Cloth" (2007)](http://www.cs.columbia.edu/cg/ESIC/esic.html) and section 7.5 will be replaced with ["Robust Treatment of Simultanious Collisions" (2008)](http://www.cs.columbia.edu/cg/RTSC/).