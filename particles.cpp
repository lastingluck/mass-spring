#include "particles.hpp"
#include "opengl.hpp"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <gsl/gsl_poly.h>
#include <unsupported/Eigen/Polynomials>

using namespace std;
using namespace Eigen;

extern std::vector<AnchorForce*> anchors;

const int m = 21, n = 21;                      // resolution of system
extern bool dc;
double mpp;         //mass per particle

double max(double a, double b) {
    return (a > b) ? a : b;
}

double min(double a, double b) {
    return (a < b) ? a : b;
}

void makeGrid(ParticleSystem *psys);

/*
* Initialize the particle system
*/
void ParticleSystem::init() {
    integrator = BACKWARD;
    wireframe = false;
    for(int i = 0; i < particles.size(); i++) {
        if(particles[i] != NULL) {
            delete particles[i];
        }
    }
    particles.clear();
    forces.clear();
    makeGrid(this); // also creates spring forces
    forces.push_back(new DragForce(this, 0.2));
    Vector3d g;
    g << 0.0, -1.0, 0.0;
    forces.push_back(new Gravity(this, g));
}

/*
* Move the system dt forward in time according to the dynamics
*/
void ParticleSystem::step(double dt, std::vector<ParticleSystem*> &cloths) {
    for(int i = 0; i < collisions.size(); i++) {
        delete collisions[i];
    }
    collisions.clear();

    // Do the internal dynamics step
    switch(integrator) {
        case FORWARD:
            for(int i = 0; i < cloths.size(); i++) {
                forwardEulerStep(cloths[i], dt);
            }
            break;
        case BACKWARD:
            for(int i = 0; i < cloths.size(); i++) {
                backwardEulerStep(cloths[i], dt);
            }
            break;
        case LEAPFROG:
            for(int i = 0; i < cloths.size(); i++) {
                leapfrogStep(cloths[i], dt);
            }
            break;
        case MIDPOINT:
            for(int i = 0; i < cloths.size(); i++) {
                midpointStep(cloths[i], dt);
            }
            break;
    }
    bool hasCollisions = false;
    //uncomment to turn off collisions
    for(int i = 0; i < cloths.size(); i++) {
        cloths[i]->updateVelocity(hasCollisions, dt);
    }
    return;

    //Update triangle normals
    for(int c1 = 0; c1 < cloths.size(); c1++) {
        for(int i = 0; i < cloths[c1]->triangles.size(); i++) {
            Triangle *t = cloths[c1]->triangles[i];
            t->normal = (t->vertices[1]->x - t->vertices[0]->x).cross(t->vertices[2]->x - t->vertices[0]->x);
            t->normal = t->normal.normalized();
        }
    }
    // 1. Find close triangles and edges (Section 6)

    // Case 1: Edge near Edge
    for(int i = 0; i < cloths.size(); i++) {
        for(int j = i; j < cloths.size(); j++) {
            for(int e1 = 0; e1 < cloths[i]->edges.size(); e1++) {
                for(int e2 = 0; e2 < cloths[j]->edges.size(); e2++) {
                    if(cloths[i]->edges[e1]->isEdgeClose(cloths[j]->edges[e2], h, this, -1)) {
                        hasCollisions = true;
                    }
                }
            }
        }
    }
    for(int i = 0; i < cloths.size(); i++) {
        for(int j = i; j < cloths.size(); j++) {
            for(int t1 = 0; t1 < cloths[i]->triangles.size(); t1++) {
                for(int p1 = 0; p1 < cloths[j]->particles.size(); p1++) {
                    if(cloths[i]->triangles[t1]->isPointClose(cloths[j]->particles[p1], h, this, -1)) {
                        hasCollisions = true;
                    }
                }
            }
        }
    }

    if(collisions.size() == 0) {
        for(int i = 0; i < cloths.size(); i++) {
            cloths[i]->updateVelocity(hasCollisions, dt);
        }
        return;
    }
    // 2. Apply Repulsion forces to collisions (Section 7.2)
    for(int i = 0; i < collisions.size(); i++) {
        Collision *c = collisions[i];
        c->inelasticForce();
        c->springForce(dt, h, 200);
    }
    //clear out collisions for the geometric processing
    for(int i = 0; i < collisions.size(); i++) {
        delete collisions[i];
    }
    collisions.clear();

    // 3. Find moving collisions (7.4)
    int iterNum = 3;
    for(int iters = 0; iters < iterNum; iters++) {
        for(int c1 = 0; c1 < cloths.size(); c1++) {
            for(int c2 = c1; c2 < cloths.size(); c2++) {
                for(int i = 0; i < cloths[c1]->edges.size(); i++) {
                    for(int j = i+1; j < cloths[c2]->edges.size(); j++) {
                        // Get vectors for polynomial
                        // (x21 + tv21) x (x31 + tv31) * (x41 + tv41) = 0
                        Edge *e1 = cloths[c1]->edges[i];
                        Edge *e2 = cloths[c2]->edges[j];
                        if(e1->sharedEdge(e2)) {
                            continue;
                        }
                        Vector3d x21 = e1->p1->x - e1->p0->x;
                        Vector3d x31 = e2->p0->x - e1->p0->x;
                        Vector3d x41 = e2->p1->x - e1->p0->x;
                        Vector3d v21 = e1->p1->aveV - e1->p0->aveV;
                        Vector3d v31 = e2->p0->aveV - e1->p0->aveV;
                        Vector3d v41 = e2->p1->aveV - e1->p0->aveV;
                        //Let the math begin (Format the polynomial in a + bx + cx^2 + dx^3 form)
                        double a = x21(1)*v31(2) + x31(2)*v21(1) - x21(2)*v31(1) - x31(1)*x21(2);
                        double b = x21(2)*v31(0) + x31(0)*v21(2) - x21(0)*v31(2) - x31(2)*x21(0);
                        double c = x21(0)*v31(1) + x31(1)*v21(0) - x21(1)*v31(0) - x31(0)*x21(1);
                        double d = v21(1)*v31(2) - v21(2)*v31(1);
                        double e = v21(2)*v31(0) - v21(0)*v31(2);
                        double f = v21(0)*v31(1) - v21(1)*v31(0);

                        double G = x21(1)*x31(2)*v41(0) - x21(2)*x31(1)*x41(0) + x41(0)*a;
                        double H = v41(0)*a + x41(0)*d;
                        double I = x21(2)*x31(0)*v41(1) - x21(0)*x31(2)*x41(1) + x41(1)*b;
                        double J = v41(1)*b + x41(1)*e;
                        double K = x21(0)*x31(1)*v41(2) - x21(1)*x31(0)*x41(2) + x41(2)*c;
                        double L = v41(2)*c + x41(2)*f;

                        //coefficients
                        double A = x21(0)*(x31(1)*x41(2)-x31(2)*x41(1)) + x21(1)*(x31(2)*x41(0)-x31(0)*x41(2)) + x21(2)*(x31(0)*x41(1)-x31(1)*x41(0));
                        double B = G + I + K;
                        double C = H + J + L;
                        double D = v41(0)*d + v41(1)*e + v41(2)*f;
                        //Need it in a + bx + cx^2 + x^3 for Eigen
                        vector<double> roots;
                        if(D > 0.00001) {
                            A /= D;
                            B /= D;
                            C /= D;
                            double r1, r2, r3;
                            int rootNum = gsl_poly_solve_cubic(C, B, A, &r1, &r2, &r3);
                            //Will always be 1 or 3 roots
                            if(r1 >= 0 && r1 <= dt) {
                                roots.push_back(r1);
                            }
                            if(rootNum == 3) {
                                if(r2 >= 0 && r2 <= dt) {
                                    roots.push_back(r2);
                                }
                                if(r3 >= 0 && r3 <= dt) {
                                    roots.push_back(r3);
                                }
                            }
                        }
                        else{
                            double r1, r2;
                            int rootNum = gsl_poly_solve_quadratic(C, B, A, &r1, &r2);
                            if(rootNum > 0) { //1 or 2 roots
                                if(r1 >= 0 && r1 <= dt) {
                                    //printf("Real Root 1: %f\n", r1);
                                    roots.push_back(r1);
                                }
                            }
                            if(rootNum == 2) {
                                if(r2 >= 0 && r2 <= dt) {
                                    //printf("Real Root 2: %f\n", r2);
                                    roots.push_back(r2);
                                }
                            }
                        }
                        // Check roots in increasing order for collision (if it equals 0)
                        std::sort(roots.begin(), roots.end());
                        for(int i = 0; i < roots.size(); i++) {
                            double t = roots[i];
                            //Register as collision
                            if(e1->isEdgeClose(e2, h, this, t)) {
                                break;
                            }
                        }
                    }
                }
                for(int t = 0; t < cloths[c1]->triangles.size(); t++) {
                    for(int p = 0; p < cloths[c2]->particles.size(); p++) {
                        // Get vectors for polynomial
                        // (x21 + tv21) x (x31 + tv31) * (x41 + tv41) = 0
                        Triangle *tri = cloths[c1]->triangles[t];
                        Particle *par = cloths[c2]->particles[p];
                        if(tri->pointInTriangle(par)) {
                            continue;
                        }
                        Vector3d x21 = tri->vertices[1]->x - tri->vertices[0]->x;
                        Vector3d x31 = tri->vertices[2]->x - tri->vertices[0]->x;
                        Vector3d x41 = par->x - tri->vertices[0]->x;
                        Vector3d v21 = tri->vertices[1]->aveV - tri->vertices[0]->aveV;
                        Vector3d v31 = tri->vertices[2]->aveV - tri->vertices[0]->aveV;
                        Vector3d v41 = par->aveV - tri->vertices[0]->aveV;
                        //Let the math begin (Format the polynomial in a + bx + cx^2 + dx^3 form)
                        double a = x21(1)*v31(2) + x31(2)*v21(1) - x21(2)*v31(1) - x31(1)*x21(2);
                        double b = x21(2)*v31(0) + x31(0)*v21(2) - x21(0)*v31(2) - x31(2)*x21(0);
                        double c = x21(0)*v31(1) + x31(1)*v21(0) - x21(1)*v31(0) - x31(0)*x21(1);
                        double d = v21(1)*v31(2) - v21(2)*v31(1);
                        double e = v21(2)*v31(0) - v21(0)*v31(2);
                        double f = v21(0)*v31(1) - v21(1)*v31(0);

                        double G = x21(1)*x31(2)*v41(0) - x21(2)*x31(1)*x41(0) + x41(0)*a;
                        double H = v41(0)*a + x41(0)*d;
                        double I = x21(2)*x31(0)*v41(1) - x21(0)*x31(2)*x41(1) + x41(1)*b;
                        double J = v41(1)*b + x41(1)*e;
                        double K = x21(0)*x31(1)*v41(2) - x21(1)*x31(0)*x41(2) + x41(2)*c;
                        double L = v41(2)*c + x41(2)*f;

                        //coefficients
                        double A = x21(0)*(x31(1)*x41(2)-x31(2)*x41(1)) + x21(1)*(x31(2)*x41(0)-x31(0)*x41(2)) + x21(2)*(x31(0)*x41(1)-x31(1)*x41(0));
                        double B = G + I + K;
                        double C = H + J + L;
                        double D = v41(0)*d + v41(1)*e + v41(2)*f;
                        //Need it in a + bx + cx^2 + x^3 for Eigen
                        vector<double> roots;
                        if(D > 0.00001) {
                            A /= D;
                            B /= D;
                            C /= D;
                            double r1, r2, r3;
                            int rootNum = gsl_poly_solve_cubic(C, B, A, &r1, &r2, &r3);
                            //Will always be 1 or 3 roots
                            if(r1 >= 0 && r1 <= dt) {
                                roots.push_back(r1);
                            }
                            if(rootNum == 3) {
                                if(r2 >= 0 && r2 <= dt) {
                                    roots.push_back(r2);
                                }
                                if(r3 >= 0 && r3 <= dt) {
                                    roots.push_back(r3);
                                }
                            }
                        }
                        else{
                            double r1, r2;
                            int rootNum = gsl_poly_solve_quadratic(C, B, A, &r1, &r2);
                            if(rootNum > 0) { //1 or 2 roots
                                if(r1 >= 0 && r1 <= dt) {
                                    roots.push_back(r1);
                                }
                            }
                            if(rootNum == 2) {
                                if(r2 >= 0 && r2 <= dt) {
                                    roots.push_back(r2);
                                }
                            }
                        }
                        // Check roots in increasing order for collision (if it equals 0)
                        std::sort(roots.begin(), roots.end());
                        for(int i = 0; i < roots.size(); i++) {
                            double t = roots[i];
                            //Register as collision
                            if(tri->isPointClose(par, h, this, t)) {
                                break;
                            }
                        }
                    }
                }
            }
        }
        if(collisions.size() == 0) {
            break;
        }
        // 4. Apply forces to geometric collisions
        for(int i = 0; i < collisions.size(); i++) {
            Collision *c = collisions[i];
            c->geometricForce(dt, h, 350);
        }
    }
    //5. Initiate Failsafe (7.5)
    for(int i = 0; i < cloths.size(); i++) {
        cloths[i]->updateVelocity(hasCollisions, dt);
    }
}

double ParticleSystem::eval(Vector3d &x21, Vector3d &x31, Vector3d &x41, Vector3d &v21, Vector3d &v31, Vector3d &v41, double t) {
    return ((x21 + t*v21).cross(x31 + t*v31)).dot(x41 + t*v41);
}

/*
* Update the velocity and position of the particles after applying all the impulses
*/
void ParticleSystem::updateVelocity(bool hasCollisions, double dt) {
    for(int i = 0; i < particles.size(); i++) {
        Particle *par = particles[i];
        par->x = par->x + dt * par->aveV;
        par->v = par->aveV;
    }
}

/*
* Combines the edges in the triangles that are the same so both point to the same instance
*/
void ParticleSystem::combineEdges() {
    bool nextTri = false;
    for(int t1 = 0; t1 < triangles.size(); t1++) {
        for(int t2 = t1+1; t2 < triangles.size(); t2++) {
            Triangle* tri1 = triangles[t1];
            Triangle* tri2 = triangles[t2];
            for(int i = 0; i < 3; i++) {
                for(int j = 0; j < 3; j++) {
                    if(tri1->edges[i]->sameEdge(tri2->edges[j])) {
                        delete tri1->edges[i];
                        tri1->edges[i] = tri2->edges[j];
                        nextTri = true; // 2 triangles can only share 1 edge
                        break;
                    }
                }
                if(nextTri) {
                    break;
                }
            }
            nextTri = false;
        }
    }
}

/*
* Finds all of the unique edges of the triangles
*/
void ParticleSystem::getUniqueEdges() {
    for(int i = 0; i < triangles.size(); i++) {
        Triangle* t = triangles[i];
        for(int j = 0; j < 3; j++) {
            if(edgeUnique(t->edges[j])) {
                edges.push_back(t->edges[j]);
            }
        }
    }
}

/*
* Checks if the edge e is already in edges
*/
bool ParticleSystem::edgeUnique(Edge* e) {
    for(int i = 0; i < edges.size(); i++) {
        if(e->sameEdge(edges[i])) {
            return false;
        }
    }
    return true;
}

/*
* Draws the system
*/
void ParticleSystem::draw(bool flip) {
    for(int i = 0; i < particles.size(); i++) {
        particles[i]->drawn = false;
    }
    for(int i = 0; i < edges.size(); i++) {
        edges[i]->drawn = false;
    }
    if(wireframe) {
        glPointSize(5);
        glColor3f(0,0,0);
        glBegin(GL_POINTS);
        for (int p = 0; p < particles.size(); p++)
            particles[p]->draw();
        glEnd();

        glLineWidth(2);
        //glColor3f(0,0,0);
        //glBegin(GL_LINES);
        //for (int f = 0; f < forces.size(); f++)
        //    forces[f]->draw();
        //glEnd();
        //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        //glBegin(GL_TRIANGLES);

        glBegin(GL_LINES);
        for(int i = 0; i < triangles.size(); i++) {
            for(int j = 0; j < 3; j++) {
                triangles[i]->edges[j]->draw();
            }
            //triangles[i]->draw();
        }
        glEnd();

        if(dc) {
            for(int i = 0; i < collisions.size(); i++) {
                collisions[i]->draw();
            }
        }
    }
    else {
        //glColor3f(1.0f, 0.0f, 0.0f);
        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        //glLineWidth(2);
        glColor3f(0,0,0);
        glBegin(GL_TRIANGLES);
        for (int f = 0; f < triangles.size(); f++)
            triangles[f]->draw();
        glEnd();
    }
}

/*
* Returns the number of positional degrees of freedom of the system
*/
int ParticleSystem::getDOFs() {
    return 3*particles.size();
}

/*
* Puts the state of the system (position and velocity) into the vectors x and v
*/
void ParticleSystem::getState(VectorXd &x, VectorXd &v) {
    for (int p = 0; p < particles.size(); p++) {
        Particle *par = particles[p];
        x.segment(p*3, 3) = par->x;
        v.segment(p*3, 3) = par->v;
    }
}

/*
* Updated candidate positions and velocities from the state vectors x and v
*/
void ParticleSystem::setState(const VectorXd &x, const VectorXd &v, double dt) {
    for (int p = 0; p < particles.size(); p++) {
        Particle *par = particles[p];
        par->candX = x.segment(p*3, 3);
        par->candV = v.segment(p*3, 3);
        par->aveV = (par->candX - par->x) / dt;
    }
}

/*
* Accumulates all outside forces into the vector f
*/
void ParticleSystem::getForces(VectorXd &f) {
    f.setZero();
    for (int i = 0; i < forces.size(); i++)
        forces[i]->addForces(f);
}

/*
* Gets the mass matrix of the system
*/
void ParticleSystem::getInertia(MatrixXd &M) {
    M.setZero();
    for (int p = 0; p < particles.size(); p++)
        M.block(p*3,p*3, 3,3) = particles[p]->m*MatrixXd::Identity(3,3);
}

/*
* Accumulates the position and velocity jacobians of the external forces into maticies Jx and Jv
*/
void ParticleSystem::getJacobians(MatrixXd &Jx, MatrixXd &Jv) {
    Jx.setZero();
    Jv.setZero();
    for (int f = 0; f < forces.size(); f++)
        forces[f]->addJacobians(Jx, Jv);
}

/*
* Draws a particle
*/
void Particle::draw() {
    if(drawn) {
        return;
    }
    //drawn = true;
    if(close) {
        glColor3f(1,0,0);
    }
    else {
        glColor3f(0,0,0);
    }
    glVertex3f(x(0), x(1), x(2));
}

/*
* Triangle constructor
*/
Triangle::Triangle(Particle *p1, Particle *p2, Particle *p3) {
    addVertices(p1, p2, p3);
    close = false;
}

/*
* Triangle destructor
*/
Triangle::~Triangle() {
    for(int i = 0; i < edges.size(); i++) {
        delete edges[i];
    }
    edges.clear();
}

/*
* Draws a triangle
*/
void Triangle::draw(bool flip) {
    Vector3d norm = (vertices[1]->x - vertices[0]->x).cross(vertices[2]->x - vertices[0]->x);
    if(flip) {
        //for(int i = 0; i < 3; i++) {
        //    edges[i]->draw();
        //}
        glNormal3f(-norm(0), -norm(1), -norm(2));
        glVertex3f(vertices[2]->x(0), vertices[2]->x(1), vertices[2]->x(2));
        glVertex3f(vertices[1]->x(0), vertices[1]->x(1), vertices[1]->x(2));
        glVertex3f(vertices[0]->x(0), vertices[0]->x(1), vertices[0]->x(2));

    }
    else {
        glNormal3f(norm(0), norm(1), norm(2));
        glVertex3f(vertices[0]->x(0), vertices[0]->x(1), vertices[0]->x(2));
        glVertex3f(vertices[1]->x(0), vertices[1]->x(1), vertices[1]->x(2));
        glVertex3f(vertices[2]->x(0), vertices[2]->x(1), vertices[2]->x(2));
    }
}

/*
* Make a triangle with the 3 particles and find the norm
*/
void Triangle::addVertices(Particle *p1, Particle *p2, Particle *p3) {
    vertices.push_back(p1);
    vertices.push_back(p2);
    vertices.push_back(p3);
    Vector3d x21 = p2->x - p1->x;
    Vector3d x31 = p3->x - p1->x;
    normal = (x31.cross(x21)).normalized();
    edges.push_back(new Edge(p1, p2));
    edges.push_back(new Edge(p2, p3));
    edges.push_back(new Edge(p3, p1));
}

/*
* Returns true if the point p is within h distance from the triangle and creates
* a collision object, returns false otherwise
*/
bool Triangle::isPointClose(Particle *p, double h, ParticleSystem *psys, double t) {
    Vector3d x1 = vertices[0]->x;
    Vector3d x2 = vertices[1]->x;
    Vector3d x3 = vertices[2]->x;
    Vector3d x4 = p->x;
    if(t >= 0) {
        x1 += vertices[0]->aveV * t;
        x2 += vertices[1]->aveV * t;
        x3 += vertices[2]->aveV * t;
        x4 += p->aveV * t;
    }
    if(!pointInTriangle(p)) {
        Vector3d x43 = x4 - x3;
        if(std::abs(x43.dot(normal)) < h) {
            Vector3d x13 = x1 - x3;
            Vector3d x23 = x2 - x3;
            //Compute barycentric coordinates
            Matrix2d A;
            Vector2d b;
            A << x13.dot(x13), x13.dot(x23), x13.dot(x23), x23.dot(x23);
            b << x13.dot(x43), x23.dot(x43);
            Vector2d bcoord = A.inverse() * b;
            double w1 = bcoord(0), w2 = bcoord(1), w3 = 1 - (w1 + w2);

            // Find characteristic length (sqrt(area))
            Vector3d x12 = x1 - x2;
            double s = (x12.norm() + x13.norm() + x23.norm()) / 2;
            double area = std::sqrt(s * (s - x12.norm()) * (s - x13.norm()) * (s - x23.norm()));
            double delta = h / std::sqrt(area);
            if((w1 >= -delta && w1 <= 1.0+delta) && (w2 >= -delta && w2 <= 1.0+delta) && (w3 >= -delta && w3 <= 1.0+delta)) {
                Collision *c = new PointTriangleCollision(p, this, w1, w2, w3);
                c->p1 = w1*x1 + w2*x2 + w3*x3;
                c->p2 = x4;
                c->t = t;
                if(normal.dot(x43) < 0) {
                    c->normal = -(c->p2-c->p1).normalized();
                }
                else {
                    c->normal = (c->p2-c->p1).normalized();
                }
                //c->v1 = w1*vertices[0]->aveV + w2*vertices[1]->aveV + w3*vertices[2]->aveV;
                //c->v2 = p->aveV;
                c->f.setZero();
                psys->collisions.push_back(c);
                return true;
            }
            return false;
        }
    }
    return false; // Returns false when the particle is one of the vertices of the triangle
}

/*
* Returns true if the 2 edges are within h distance apart from each other and
* creates a collision object, returns false otherwise
*/
bool Edge::isEdgeClose(Edge* e, double h, ParticleSystem *psys, double t) {
    Vector3d x1 = p0->x;
    Vector3d x2 = p1->x;
    Vector3d x3 = e->p0->x;
    Vector3d x4 = e->p1->x;
    if(t >= 0) {
        x1 += p0->aveV * t;
        x2 += p1->aveV * t;
        x3 += e->p0->aveV * t;
        x4 += e->p1->aveV * t;
    }
    if(!sharedEdge(e)) {
        Vector3d x21 = x2 - x1;
        Vector3d x43 = x4 - x3;
        //check for parallel edges
        if((x21.cross(x43)).norm() < 0.001) {
            Vector3d x41 = x4 - x1;
            Vector3d x13 = x1 - x3;
            double sN = 0, sD = 1, tN = x41.dot(x13), tD = x41.dot(x41);
            if(tN < 0) {
                tN = 0;
                if(-x21.dot(x13) < 0) {
                    sN = 0;
                }
                else if(-x21.dot(x13) > x21.dot(x21)) {
                    sN = sD;
                }
                else {
                    sN = -x21.dot(x13);
                    sD = x21.dot(x21);
                }
            }
            else if(tN > tD) {
                tN = tD;
                if(-x21.dot(x13) + x21.dot(x41) < 0) {
                    sN = 0;
                }
                else if(-x21.dot(x13) + x21.dot(x41) > x21.dot(x21)) {
                    sN = sD;
                }
                else {
                    sN = -x21.dot(x13) + x21.dot(x41);
                    sD = x21.dot(x21);
                }
            }
            double sc, tc;
            if(std::abs(sN) < 0.00001) {
                sc = 0;
            }
            else {
                sc = sN / sD;
            }
            if(std::abs(tN) < 0.00001) {
                tc = 0;
            }
            else {
                tc = tN / tD;
            }
            Vector3d c1 = (x1 - x3) + (sc * x21) - (tc * x43);
            double c1d = c1.norm();
            if(c1d < h) {
                Collision *c = new EdgeEdgeCollision(this, e, sc, tc);
                c->normal = c1.normalized();
                //c->normal = (e->p0->x + tc*x43) - (p0->x + sc*x21);
                //c->normal = c->normal.normalized();
                c->p1 = x1 + sc*x21;
                c->p2 = x3 + tc*x43;
                c->t = t;
                //c->v1 = p0->aveV + sc*(p1->aveV - p0->aveV);
                //c->v2 = e->p0->aveV + tc*(e->p1->aveV - e->p0->aveV);
                c->f.setZero();
                psys->collisions.push_back(c);
                return true;
            }
            return false;
        }
        //find closest points
        Matrix2d A;
        Vector2d b;
        Vector3d x31 = x3 - x1;
        A << x21.dot(x21), -x21.dot(x43), -x21.dot(x43), x43.dot(x43);
        b << x21.dot(x31), -x43.dot(x31);
        Vector2d w = A.inverse() * b;

        if(w(0) >= 0 && w(0) <= 1 && w(1) >= 0 && w(1) <= 1) {
            Vector3d px1 = x1 + w(0)*x21;
            Vector3d px2 = x3 + w(1)*x43;
            if((px2-px1).norm() < h) {
                Collision *c = new EdgeEdgeCollision(this, e, w(0), w(1));
                c->normal = (px2-px1).normalized();
                c->p1 = px1;
                c->p2 = px2;
                c->t = t;
                //c->v1 = p0->aveV + w(0)*(p1->aveV - p0->aveV);
                //c->v2 = e->p0->aveV + w(1)*(e->p1->aveV - e->p0->aveV);
                c->f.setZero();
                psys->collisions.push_back(c);
                return true;
            }
            return false;
        }
        else {
            //clamping distance
            double d0 = (w(0) < 0) ? -w(0) : (w(0) > 1) ? w(0)-1 : 0;
            double d1 = (w(1) < 0) ? -w(1) : (w(1) > 1) ? w(1)-1 : 0;
            Vector3d p, p2;
            double a, b;
            Edge *e1, *e2;
            if(d0 > d1) {
                a = (w(0) < 0) ? w(0)+d0 : w(0)-d0; //Clamped point
                //Project point onto line
                p = x1 + a*x21;
                Vector3d x3p = p - x3;
                b = x3p.dot(x43);
                b = (b < 0) ? 0 : (b > 1) ? 1 : b;
                p2 = x3 + b*x43;
                e1 = this;
                e2 = e;
            }
            else {
                a = (w(1) < 0) ? w(1)+d1 : w(1)-d1; //Clamped point
                //Project point onto line
                p = x3 + a*x43;
                Vector3d x1p = p - x1;
                b = x1p.dot(x21);
                b = (b < 0) ? 0 : (b > 1) ? 1 : b;
                p2 = x1 + b*x21;
                e1 = e;
                e2 = this;
            }
            if((p2-p).norm() < h) {
                Collision *c = new EdgeEdgeCollision(e1, e2, a, b);
                c->normal = (p2-p).normalized();
                c->p1 = p;
                c->p2 = p2;
                c->t = t;
                c->f.setZero();
                psys->collisions.push_back(c);
                return true;
            }
            return false;
        }
    }
    return false;
}

/*
* Draws a triangle edge
*/
void Edge::draw() {
    //if(drawn) {
    //    return;
    //}
    //drawn = true;
    if(close) {
        glColor3f(1,0,0);
    }
    else {
        glColor3f(0,0,0);
    }
    glVertex3f(p0->x(0), p0->x(1), p0->x(2));
    glVertex3f(p1->x(0), p1->x(1), p1->x(2));
}

/*
* Returns true if the edge e is the same as the current edge
*/
bool Edge::sameEdge(Edge *e) {
    return (p0->i == e->p0->i || p0->i == e->p1->i) && (p1->i == e->p0->i || p1->i == e->p1->i);
}

/*
* Returns true if the edge e shares a vertex with the current edge
*/
bool Edge::sharedEdge(Edge *e) {
    return (p0->i == e->p0->i || p0->i == e->p1->i) || (p1->i == e->p0->i || p1->i == e->p1->i);
}

/*
* Returns true if the triangle has the vertex p
*/
bool Triangle::pointInTriangle(Particle *p) {
    return vertices[0]->sameParticle(p) || vertices[1]->sameParticle(p) || vertices[2]->sameParticle(p);
}

/*
* Draws the triangle and point in a different color so it can be seen
*/
void PointTriangleCollision::draw() {
    /*
    //glColor3f(0,0,1);
    glBegin(GL_POINTS);
    glVertex3f(point->x(0), point->x(1), point->x(2));
    glVertex3f(triangle->edges[0]->p0->x(0), triangle->edges[0]->p0->x(1), triangle->edges[0]->p0->x(2));
    glVertex3f(triangle->edges[1]->p0->x(0), triangle->edges[1]->p0->x(1), triangle->edges[1]->p0->x(2));
    glVertex3f(triangle->edges[2]->p0->x(0), triangle->edges[2]->p0->x(1), triangle->edges[2]->p0->x(2));
    glEnd();

    glBegin(GL_LINE_STRIP);
    glVertex3f(triangle->edges[0]->p0->x(0), triangle->edges[0]->p0->x(1), triangle->edges[0]->p0->x(2));
    glVertex3f(triangle->edges[0]->p1->x(0), triangle->edges[0]->p1->x(1), triangle->edges[0]->p1->x(2));
    glVertex3f(triangle->edges[1]->p1->x(0), triangle->edges[1]->p1->x(1), triangle->edges[1]->p1->x(2));
    glVertex3f(triangle->edges[2]->p1->x(0), triangle->edges[2]->p1->x(1), triangle->edges[2]->p1->x(2));
    glEnd();
    */
    glColor3f(0,1,0);
    glBegin(GL_LINES);
    glVertex3f(p1(0), p1(1), p1(2));
    glVertex3f(p1(0)+f(0), p1(1)+f(1), p1(2)+f(2));
    glVertex3f(p2(0), p2(1), p2(2));
    glVertex3f(p2(0)-f(0), p2(1)-f(1), p2(2)-f(2));
    glEnd();
}

void PointTriangleCollision::addImpulse(double impulse, Vector3d &norm, bool swap, std::string &print) {
    f += norm * impulse;
    double in = 2.0*impulse / (1.0+w1*w1+w2*w2+w3*w3);
    double m = point->m;
    std::ostringstream out;
    out << std::fixed << std::setprecision(10) << "Impulse: " << in << ", (" << w1*(in/m) << ", " << w2*(in/m) << ", " << w3*(in/m) << ")\n";
    out << std::fixed << std::setprecision(10) << "Normal: (" << norm(0) << ", " << norm(1) << ", " << norm(2) << "), " << norm.norm() << "\n";
    out << std::fixed << std::setprecision(10) << "v1 (" << triangle->vertices[0]->aveV(0) << ", " << triangle->vertices[0]->aveV(1) << ", " << triangle->vertices[0]->aveV(2) << ") -> ";
    if(!swap) {
        triangle->vertices[0]->aveV += w1 * (in / m) * norm;
    }
    else {
        triangle->vertices[0]->aveV -= w1 * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << triangle->vertices[0]->aveV(0) << ", " << triangle->vertices[0]->aveV(1) << ", " << triangle->vertices[0]->aveV(2) << ")\n";
    out << std::fixed << std::setprecision(10) << "v2 (" << triangle->vertices[1]->aveV(0) << ", " << triangle->vertices[1]->aveV(1) << ", " << triangle->vertices[1]->aveV(2) << ") -> ";
    if(!swap) {
        triangle->vertices[1]->aveV += w2 * (in / m) * norm;
    }
    else {
        triangle->vertices[1]->aveV -= w2 * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << triangle->vertices[1]->aveV(0) << ", " << triangle->vertices[1]->aveV(1) << ", " << triangle->vertices[1]->aveV(2) << ")\n";
    out << std::fixed << std::setprecision(10) << "v3 (" << triangle->vertices[2]->aveV(0) << ", " << triangle->vertices[2]->aveV(1) << ", " << triangle->vertices[2]->aveV(2) << ") -> ";
    if(!swap) {
        triangle->vertices[2]->aveV += w3 * (in / m) * norm;
    }
    else {
        triangle->vertices[2]->aveV -= w3 * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << triangle->vertices[2]->aveV(0) << ", " << triangle->vertices[2]->aveV(1) << ", " << triangle->vertices[2]->aveV(2) << ")\n";
    out << std::fixed << std::setprecision(10) << "v4 (" << point->aveV(0) << ", " << point->aveV(1) << ", " << point->aveV(2) << ") -> ";
    if(!swap) {
        point->aveV -= (in / m) * norm;
    }
    else {
        point->aveV += (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << point->aveV(0) << ", " << point->aveV(1) << ", " << point->aveV(2) << ")\n";
    print = out.str();
}

void PointTriangleCollision::inelasticForce() {
    Vector3d relV;
    bool normSwap = false;
    v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
    v2 = point->aveV;
    if(v1.norm() > v2.norm()) {
        relV = v1 - v2;
    }
    else {
        relV = v2 - v1;
        normSwap = true;
    }
    double relVn = relV.dot(normal);
    if(relVn < 0.0001) {
        double imp = -(mpp * relVn / 2.0);
        double oldrv = relVn;
        Vector3d oldr = relV;
        Vector3d ov1 = v1;
        Vector3d ov2 = v2;
        std::string debug;
        addImpulse(imp, normal, normSwap, debug);
        v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
        v2 = point->aveV;
        relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
        relVn = relV.dot(normal);
        if(std::abs(relVn) > 0.001) {
            printf("\nInelastic Impulse PT: relVn %.10g, imp: %.10g\n", oldrv, imp);
            printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            std::cout << debug;
            printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            printf("New relV: %.10g\n\n", relVn);
            std::exit(0);
        }
    }
}

void PointTriangleCollision::springForce(double dt, double h, double k) {
    double d = (h+h/2.0) - (p2-p1).dot(normal);
    Vector3d relV;
    v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
    v2 = point->aveV;
    if(v1.norm() > v2.norm()) {
        relV = v1 - v2;
    }
    else {
        relV = v2 - v1;
    }
    double relVn = relV.dot(normal);
    if(relVn < 0.1 * d / dt) {
        double imp = -std::min(dt*k*d, mpp*(0.1*d/dt - relVn));
        //double oldrv = relVn;
        //Vector3d oldr = relV;
        //Vector3d ov1 = v1;
        //Vector3d ov2 = v2;
        std::string debug;
        addImpulse(imp, normal, false, debug);
        v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
        v2 = point->aveV;
        /*
        Vector3d relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
        double relVn = relV.dot(normal);
        if(std::abs(relVn) > 0.1*d/dt+0.01) {
            printf("\nSpring Impulse PT: relVn %.10g, imp: %.10g, crit: %.10g\n", oldrv, imp, 0.1*d/dt);
            printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            std::cout << debug;
            printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            printf("New relV: %.10g\n\n", relVn);
            //std::exit(0);
        }
        */
    }
}

void PointTriangleCollision::geometricForce(double dt, double h, double k) {
    Vector3d relV;
    bool normSwap = false;
    v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
    v2 = point->aveV;
    if(v1.norm() > v2.norm()) {
        relV = v1 - v2;
    }
    else {
        relV = v2 - v1;
        normSwap = true;
    }
    double relVn = relV.dot(normal);
    //If geometry is approaching, apply inelastic impulse, otherwise apply spring forces
    if(relVn < 0) { //moving towards each other
        //apply Inelastic impulse
        double imp = -(mpp * relVn / 2.0);
        double oldrv = relVn;
        Vector3d oldr = relV;
        Vector3d ov1 = v1;
        Vector3d ov2 = v2;
        std::string debug;
        addImpulse(imp, normal, normSwap, debug);
        v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
        v2 = point->aveV;
        relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
        relVn = relV.dot(normal);
        if(std::abs(relVn) > 0.001) {
            printf("\nInelastic Geometric Impulse PT: relVn %.10g, imp: %.10g\n", oldrv, imp);
            printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            std::cout << debug;
            printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            printf("New relV: %.10g\n\n", relVn);
            std::exit(0);
        }
    }
    else {  //moving away
        //apply spring repulsion force
        //Vector3d op1 = w1*triangle->vertices[0]->x + w2*triangle->vertices[1]->x + w3*triangle->vertices[2]->x;
        double d;
        //if((point->x - op1).norm() > h) {
        //    d = (point->x-op1).dot(normal) - (h+h/2.0);
        //}
        //else {
        //    d = (h+h/2.0) - (point->x-op1).dot(normal);
        //}
        d = (h+h/2.0) - (p2-p1).dot(normal);
        if(relVn < 0.1*d / dt) {
            double imp = -std::min(dt*k*d, mpp*(0.1*d/dt - relVn));
            //double oldrv = relVn;
            //Vector3d oldr = relV;
            //Vector3d ov1 = v1;
            //Vector3d ov2 = v2;
            std::string debug;
            addImpulse(imp, normal, true, debug);
            v1 = w1*triangle->vertices[0]->aveV + w2*triangle->vertices[1]->aveV + w3*triangle->vertices[2]->aveV;
            v2 = point->aveV;
            //Vector3d relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
            //double relVn = relV.dot(normal);
            //if(std::abs(relVn) > 0.1*d/dt+0.01) {
            //    printf("\nSpring Impulse PT: relVn %.10g, imp: %.10g, crit: %.10g\n", oldrv, imp, 0.1*d/dt);
            //    printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            //    printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            //    printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            //    std::cout << debug;
            //    printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            //    printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            //    printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            //    printf("New relV: %.10g\n\n", relVn);
                //std::exit(0);
            //}
        }
    }
}

void EdgeEdgeCollision::draw() {
    /*
    //glColor3f(0,1,0);
    glBegin(GL_POINTS);
    glVertex3f(edge1->p0->x(0), edge1->p0->x(1), edge1->p0->x(2));
    glVertex3f(edge1->p1->x(0), edge1->p1->x(1), edge1->p1->x(2));
    glVertex3f(edge2->p0->x(0), edge2->p0->x(1), edge2->p0->x(2));
    glVertex3f(edge2->p1->x(0), edge2->p1->x(1), edge2->p1->x(2));
    glEnd();
    */
    glBegin(GL_LINES);
    /*
    glVertex3f(edge1->p0->x(0), edge1->p0->x(1), edge1->p0->x(2));
    glVertex3f(edge1->p1->x(0), edge1->p1->x(1), edge1->p1->x(2));
    glVertex3f(edge2->p0->x(0), edge2->p0->x(1), edge2->p0->x(2));
    glVertex3f(edge2->p1->x(0), edge2->p1->x(1), edge2->p1->x(2));
    */
    glColor3f(0,1,0);
    glVertex3f(p1(0), p1(1), p1(2));
    glVertex3f(p1(0)+f(0), p1(1)+f(1), p1(2)+f(2));
    glVertex3f(p2(0), p2(1), p2(2));
    glVertex3f(p2(0)-f(0), p2(1)-f(1), p2(2)-f(2));
    glEnd();
}

void EdgeEdgeCollision::addImpulse(double impulse, Vector3d &norm, bool swap, std::string &print) {
    f += norm * impulse;
    double in = 2.0 * impulse / (a*a + (1-a)*(1-a) + b*b + (1-b)*(1-b));
    double m = edge1->p0->m;
    std::ostringstream out;
    out << std::fixed << std::setprecision(10) << "Impulse: " << in << ", (" << (1-a)*(in/m) << ", " << a*(in/m) << ", " << (1-b)*(in/m) << ", " << b*(in/m) << ")\n";
    out << std::fixed << std::setprecision(10) << "Normal: (" << norm(0) << ", " << norm(1) << ", " << norm(2) << "), " << norm.norm() << "\n";
    out << std::fixed << std::setprecision(10) << "v1 (" << edge1->p0->aveV(0) << ", " << edge1->p0->aveV(1) << ", " << edge1->p0->aveV(2) << ") -> ";
    if(!swap) {
        edge1->p0->aveV += (1-a) * (in / m) * norm;
    }
    else {
        edge1->p0->aveV -= (1-a) * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << edge1->p0->aveV(0) << ", " << edge1->p0->aveV(1) << ", " << edge1->p0->aveV(2) << ")\n";
    out << std::fixed << std::setprecision(10) << "v2 (" << edge1->p1->aveV(0) << ", " << edge1->p1->aveV(1) << ", " << edge1->p1->aveV(2) << ") -> ";
    if(!swap) {
        edge1->p1->aveV += a * (in / m) * norm;
    }
    else {
        edge1->p1->aveV -= a * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << edge1->p1->aveV(0) << ", " << edge1->p1->aveV(1) << ", " << edge1->p1->aveV(2) << ")\n";
    out << std::fixed << std::setprecision(10) << "v3 (" << edge2->p0->aveV(0) << ", " << edge2->p0->aveV(1) << ", " << edge2->p0->aveV(2) << ") -> ";
    if(!swap) {
        edge2->p0->aveV -= (1-b) * (in / m) * norm;
    }
    else {
        edge2->p0->aveV += (1-b) * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << edge2->p0->aveV(0) << ", " << edge2->p0->aveV(1) << ", " << edge2->p0->aveV(2) << ")\n";
    out << std::fixed << std::setprecision(10) << "v4 (" << edge2->p1->aveV(0) << ", " << edge2->p1->aveV(1) << ", " << edge2->p1->aveV(2) << ") -> ";
    if(!swap) {
        edge2->p1->aveV -= b * (in / m) * norm;
    }
    else {
        edge2->p1->aveV += b * (in / m) * norm;
    }
    out << std::fixed << std::setprecision(10) << "(" << edge2->p1->aveV(0) << ", " << edge2->p1->aveV(1) << ", " << edge2->p1->aveV(2) << ")\n";
    print = out.str();
}

void EdgeEdgeCollision::inelasticForce() {
    Vector3d relV;
    bool normSwap = false;
    v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
    v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);
    if(v1.norm() > v2.norm()) {
        relV = v1 - v2;
    }
    else {
        relV = v2 - v1;
        normSwap = true;
    }
    double relVn = relV.dot(normal);
    if(relVn < 0.0001) {
        double imp = -(mpp * relVn / 2.0);
        double oldrv = relVn;
        Vector3d oldr = relV;
        Vector3d ov1 = v1;
        Vector3d ov2 = v2;
        std::string debug;
        addImpulse(imp, normal, normSwap, debug);
        v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
        v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);
        relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
        relVn = relV.dot(normal);
        if(std::abs(relVn) > 0.001) {
            printf("\nInelastic Impulse EE: relVn %.10g, imp: %.10g\n", oldrv, imp);
            printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            std::cout << debug;
            printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            printf("New relV: %.10g\n\n", relVn);
            std::exit(0);
        }
        else {
            //printf("Inelastic 0\n");
        }
    }
}

void EdgeEdgeCollision::springForce(double dt, double h, double k) {
    double d = (h+h/2.0) - (p2-p1).dot(normal);
    Vector3d relV;
    v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
    v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);
    if(v1.norm() > v2.norm()) {
        relV = v1 - v2;
    }
    else {
        relV = v2 - v1;
    }
    double relVn = relV.dot(normal);
    if(relVn < 0.1 * d / dt) {
        double imp = -std::min(dt*k*d, mpp*(0.1*d/dt - relVn));
        //double oldrv = relVn;
        //Vector3d oldr = relV;
        //Vector3d ov1 = v1;
        //Vector3d ov2 = v2;
        std::string debug;
        addImpulse(imp, normal, false, debug);
        v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
        v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);
        /*
        Vector3d relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
        double relVn = relV.dot(normal);
        if(std::abs(relVn) > 0.1*d/dt+0.01) {
            printf("\nSpring Impulse EE: relVn %.10g, imp: %.10g, crit: %.10g\n", oldrv, imp, 0.1*d/dt);
            printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            std::cout << debug;
            printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            printf("New relV: %.10g\n\n", relVn);
            //std::exit(0);
        }
        */
    }
}

void EdgeEdgeCollision::geometricForce(double dt, double h, double k) {
    Vector3d relV;
    bool normSwap = false;
    v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
    v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);
    if(v1.norm() > v2.norm()) {
        relV = v1 - v2;
    }
    else {
        relV = v2 - v1;
        normSwap = true;
    }
    double relVn = relV.dot(normal);
    //If geometry is approaching, apply inelastic impulse, otherwise apply spring forces
    if(relVn < 0) { //moving towards each other
        //apply Inelastic impulse
        double imp = -(mpp * relVn / 2.0);
        double oldrv = relVn;
        Vector3d oldr = relV;
        Vector3d ov1 = v1;
        Vector3d ov2 = v2;
        std::string debug;
        addImpulse(imp, normal, normSwap, debug);
        v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
        v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);
        relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
        relVn = relV.dot(normal);
        if(std::abs(relVn) > 0.001) {
            printf("\nInelastic Geometric Impulse EE: relVn %.10g, imp: %.10g\n", oldrv, imp);
            printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            std::cout << debug;
            printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            printf("New relV: %.10g\n\n", relVn);
            std::exit(0);
        }
    }
    else {  //moving away
        //apply spring repulsion
        //Vector3d op1 = edge1->p0->x + a*(edge1->p1->x - edge1->p0->x);
        //Vector3d op2 = edge2->p0->x + b*(edge2->p1->x - edge2->p0->x);
        double d;
        //if((op2-op1).norm() > h) {
        //    d = (op2-op1).dot(normal) - (h+h/2.0);
        //}
        //else {
        //    d = (h+h/2.0) - (op2-op1).dot(normal);
        //}
        d = (h+h/2.0) - (p2-p1).dot(normal);
        if(relVn < 0.1*d / dt) {
            double imp = -std::min(dt*k*d, mpp*(0.1*d/dt - relVn));
            //double oldrv = relVn;
            //Vector3d oldr = relV;
            //Vector3d ov1 = v1;
            //Vector3d ov2 = v2;
            std::string debug;
            addImpulse(imp, normal, true, debug);
            v1 = edge1->p0->aveV + a*(edge1->p1->aveV - edge1->p0->aveV);
            v2 = edge2->p0->aveV + b*(edge2->p1->aveV - edge2->p0->aveV);

            //Vector3d relV = (v1.norm() > v2.norm()) ? v1-v2 : v2-v1;
            //double relVn = relV.dot(normal);
            //if(std::abs(relVn) > 0.1*d/dt+0.01) {
            //    printf("\nSpring Geometric Impulse EE: relVn %.10g, imp: %.10g, crit: %.10g\n", oldrv, imp, 0.1*d/dt);
            //    printf("RVel: (%.10g, %.10g, %.10g)\n", oldr(0), oldr(1), oldr(2));
            //    printf("Vel 1: (%.10g, %.10g, %.10g), %.10g\n", ov1(0), ov1(1), ov1(2), ov1.norm());
            //    printf("Vel 2: (%.10g, %.10g, %.10g), %.10g\n", ov2(0), ov2(1), ov2(2), ov2.norm());
            //    std::cout << debug;
            //    printf("New Vel 1: (%.10g, %.10g, %.10g), %.10g\n", v1(0), v1(1), v1(2), v1.norm());
            //    printf("New Vel 2: (%.10g, %.10g, %.10g), %.10g\n", v2(0), v2(1), v2(2), v2.norm());
            //    printf("New RVel: (%.10g, %.10g, %.10g)\n", relV(0), relV(1), relV(2));
            //    printf("New relV: %.10g\n\n", relVn);
                //std::exit(0);
            //}
        }
    }
}

void Gravity::addForces(VectorXd &f) {
    for(int i = 0; i < ps->particles.size(); i++) {
        f.segment(3*i, 3) += g;
    }
}

void Gravity::addJacobians(MatrixXd &Jx, MatrixXd &Jv) {

}

void Gravity::draw() {

}

void DragForce::addForces(VectorXd &f) {
    for (int p = 0; p < ps->particles.size(); p++) {
        Particle *particle = ps->particles[p];
        f.segment(3*p, 3) += -kd*particle->v;
    }
}

void DragForce::addJacobians(MatrixXd &Jx, MatrixXd &Jv) {
    for (int p = 0; p < ps->particles.size(); p++)
        Jv.block(3*p,3*p, 3,3) += -kd*MatrixXd::Identity(3, 3);
}

void DragForce::draw() {
}

void SpringForce::addForces(VectorXd &f) {
    if(p0 == NULL || p1 == NULL) {
        return;
    }

    Vector3d i = p0->x - p1->x;
    Vector3d ii = p0->v - p1->v;
    double inorm = i.norm();
    double idot = ii.dot(i);
    Vector3d fa = -(ks*(inorm-l0) + kd*(idot/inorm)) * (i/inorm);
    f.segment(3*p0->i, 3) += fa;
    f.segment(3*p1->i, 3) -= fa;
}

void SpringForce::addJacobians(MatrixXd &Jx, MatrixXd &Jv) {
    if(p0 == NULL || p1 == NULL) {
        return;
    }

    Vector3d xij = p0->x - p1->x;
    Vector3d xijn = xij.normalized();
    Matrix3d jx = -ks * (max(1 - l0/xij.norm(), 0) * (MatrixXd::Identity(3, 3) - xijn*xijn.transpose()) + (xijn*xijn.transpose()));
    Matrix3d jv = -kd * xij.normalized() * (xij.normalized()).transpose();
    Jx.block(3*p0->i, 3*p0->i, 3, 3) += jx;
    Jv.block(3*p0->i, 3*p0->i, 3, 3) += jv;

    Jx.block(3*p0->i, 3*p1->i, 3, 3) -= jx;
    Jv.block(3*p0->i, 3*p1->i, 3, 3) -= jv;

    Jx.block(3*p1->i, 3*p0->i, 3, 3) -= jx;
    Jv.block(3*p1->i, 3*p0->i, 3, 3) -= jv;

    Jx.block(3*p1->i, 3*p1->i, 3, 3) += jx;
    Jv.block(3*p1->i, 3*p1->i, 3, 3) += jv;
}

void SpringForce::draw() {
    glVertex3f(p0->x(0), p0->x(1), p0->x(2));
    glVertex3f(p1->x(0), p1->x(1), p1->x(2));
}

void BendForce::addForces(VectorXd &f) {
    if(p0 == NULL || p1 == NULL || p2 == NULL) {
        return;
    }
}

void BendForce::addJacobians(MatrixXd &Jx, MatrixXd &Jv) {
    if(p0 == NULL || p1 == NULL || p2 == NULL) {
        return;
    }
}

void BendForce::draw() {

}

void AnchorForce::addForces(VectorXd &f) {
    if (p == NULL)
        return;
    Vector3d dx = p->x - x;
    f.segment(3*p->i, 3) += -ks*dx - kd*p->v;
}

void AnchorForce::addJacobians(MatrixXd &Jx, MatrixXd &Jv) {
    if (p == NULL)
        return;
    Jx.block(3*p->i,3*p->i, 3,3) += -ks*MatrixXd::Identity(3, 3);
    Jv.block(3*p->i,3*p->i, 3,3) += -kd*MatrixXd::Identity(3, 3);
}

void AnchorForce::draw() {

}

void makeGrid(ParticleSystem *psys) {
    //psys->h = 0.02;
    double pmass = psys->mass/((m + 1)*(n + 1));         // mass per particle
    mpp = pmass;
    //double z0 = sqrt((psys->x0)*(psys->x0)+(psys->y0)*(psys->y0)), z1 = sqrt((psys->x1)*(psys->x1)+(psys->y1)*(psys->y1));
    double dx = (psys->x1 - psys->x0)/m, dy = (psys->y1 - psys->y0)/n;     // lengths of springs
    //double dz = (z1 - z0)/m;
    double dz = std::sqrt(dx*dx + dy*dy);
    //printf("x0: %f, y0: %f, z0: %f\n", psys->x0, psys->y0, z0);
    //printf("dx: %f, dy: %f, dz: %f\n", dx, dy, dz);
    double ks = 1500, kd = 10;                       // spring constant, damping
    double ks2 = 800, kd2 = 6;                     // shear spring constants
    double ks3 = 400, kd3 = 0.5;                   // bend spring constants
    // create particles
    Particle *particles[m][n];
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            Vector3d x, v;
            x << psys->x0+dx*i, 0, psys->y0+dy*j;
            v << 0, 0, 0;
            //printf("X: (%f, %f, %f)\n", x(0), x(1), x(2));
            int index = psys->particles.size();
            particles[i][j] = new Particle(index, pmass, x, v);
            psys->particles.push_back(particles[i][j]);
        }
    }
    // group particles into triangles
    for(int i = 0; i < m-1; i++) {
        for(int j = 0; j < n-1; j++) {
            psys->triangles.push_back(new Triangle(particles[i][j], particles[i+1][j], particles[i][j+1]));
            psys->triangles.push_back(new Triangle(particles[i+1][j+1], particles[i][j+1], particles[i+1][j]));
            //psys->triangles[psys->triangles.size()-2]->close = false;
            //psys->triangles[psys->triangles.size()-1]->close = false;
        }
    }
    psys->combineEdges();
    psys->getUniqueEdges();
    // create springs
    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            Particle *p0 = particles[i][j];
            if (i < m-1) {
                Particle *p1 = particles[i+1][j];
                psys->forces.push_back(new SpringForce(p0, p1, ks, kd, dx));
            }
            if (j < n-1) {
                Particle *p1 = particles[i][j+1];
                psys->forces.push_back(new SpringForce(p0, p1, ks, kd, dy));
            }
            if (i < m-1 && j < n-1) { //shear springs
                Particle *p1 = particles[i+1][j+1], *p2 = particles[i+1][j], *p3 = particles[i][j+1];
				psys->forces.push_back(new SpringForce(p0, p1, ks2, kd2, dz));
				psys->forces.push_back(new SpringForce(p2, p3, ks2, kd2, dz));
			}
            if(i < m-2) { //bend springs
                Particle *p1 = particles[i+2][j];
                psys->forces.push_back(new SpringForce(p0, p1, ks3, kd3, dx+dx));
            }
            if(j < n-2) { //bend springs
                Particle *p1 = particles[i][j+2];
                psys->forces.push_back(new SpringForce(p0, p1, ks3, kd3, dy+dy));
            }
            if((i == 0 && j == 0) || (i == m-1 && j == n-1)) {
                printf("(%f, %f, %f)\n", p0->x(0), p0->x(1), p0->x(2));
                AnchorForce *tmp = new AnchorForce(p0, p0->x, 10000, 10);
                psys->forces.push_back(tmp);
                anchors.push_back(tmp);
            }
        }
    }
}
