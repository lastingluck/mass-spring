#ifndef PARTICLES_HPP
#define PARTICLES_HPP

#include "integration.hpp"
#include <Eigen/Dense>
#include <vector>
#include <cstdio>

using namespace Eigen;

class Particle;
class Force;
class Triangle;
class Edge;
class Collision;

enum IntegratorType {
    FORWARD,
    BACKWARD,
    LEAPFROG,
    MIDPOINT
};

class ParticleSystem: public PhysicalSystem {
public:
    enum IntegratorType integrator;
    std::vector<Particle*> particles;
    std::vector<Force*> forces;
    std::vector<Triangle*> triangles;
    std::vector<Edge*> edges;
    std::vector<Collision*> collisions;

    bool wireframe;
    double h;
    double mass;
    double x0, x1, y0, y1;

    ParticleSystem(): h(0.02), mass(10), x0(0.0), x1(1.1), y0(0.0), y1(1.1) {}
    ParticleSystem(double h, double m, double x0, double x1, double y0, double y1):
            h(h), mass(m), x0(x0), x1(x1), y0(y0), y1(y1) {}
    void init();          // initialize the system
    void step(double dt, std::vector<ParticleSystem*> &cloths); // perform a time step of length dt
    void draw(bool flip);          // draw everything
    // PhysicalSystem functions, see integration.hpp
    int getDOFs();
    void getState(VectorXd &x, VectorXd &v);
    void setState(const VectorXd &x, const VectorXd &v, double dt);
    void getInertia(MatrixXd &M);
    void getForces(VectorXd &f);
    void getJacobians(MatrixXd &Jx, MatrixXd &Jv);
    void getUniqueEdges();
    void combineEdges();
    bool edgeUnique(Edge* e);
    void updateVelocity(bool hasCollisions, double dt);
    double eval(Vector3d &x21, Vector3d &x31, Vector3d &x41, Vector3d &v21, Vector3d &v31, Vector3d &v41, double t);
};

class Particle {
public:
    int i;      // index
    double m;   // mass
    Vector3d x; // position
    Vector3d candX;
    Vector3d v; // velocity
    Vector3d candV;
    Vector3d aveV;
    Vector3d f; // force

    // Used for some debugging purposes, probably won't be used anymore
    bool close;
    bool drawn;

    Particle(int i, double m, Vector3d &x, Vector3d &v): i(i), m(m), x(x), v(v), close(false) {}
    Particle(const Particle &p): i(p.i), m(p.m), x(p.x), candX(p.candX), v(p.v), candV(p.candV), aveV(p.aveV), f(p.f), close(false), drawn(false) {}
    void draw();
    bool sameParticle(Particle* p) { return i == p->i; }
};

class Triangle {
public:
    std::vector<Particle*> vertices;
    std::vector<Edge*> edges;
    Vector3d normal;

    bool close;

    Triangle(Particle *p1, Particle *p2, Particle *p3);
    ~Triangle();
    void draw(bool flip = false);
    void addVertices(Particle *p1, Particle *p2, Particle *p3);
    void setNormal(Vector3d &norm) { normal = norm; }
    bool isPointClose(Particle *p, double h, ParticleSystem *psys, double t);
    bool pointInTriangle(Particle *p);
};

class Edge {
public:
    Particle *p0;
    Particle *p1;

    bool close;
    bool drawn;

    Edge(Particle *p0, Particle *p1): p0(p0), p1(p1), close(false) {}
    ~Edge() { p0 = p1 = NULL; }
    bool isEdgeClose(Edge* e, double h, ParticleSystem *psys, double t);
    bool sameEdge(Edge *e);
    bool sharedEdge(Edge *e);
    void draw();
};

class Collision {
public:
    Vector3d normal;
    Vector3d v1;
    Vector3d v2;
    Vector3d p1;
    Vector3d p2;
    double dvn;
    double t; // time of collision
    Vector3d f;

    virtual ~Collision() {}
    virtual void draw() = 0;
    virtual void addImpulse(double impulse, Vector3d &norm, bool swap, std::string &print) = 0;
    virtual void inelasticForce() = 0;
    virtual void springForce(double dt, double h, double k) = 0;
    virtual void geometricForce(double dt, double h, double k) = 0;
};

class PointTriangleCollision: public Collision {
public:
    Particle *point;
    Triangle *triangle;
    double w1, w2, w3;

    PointTriangleCollision(Particle *p, Triangle *t, double w1, double w2, double w3): point(p), triangle(t), w1(w1), w2(w2), w3(w3) {}
    ~PointTriangleCollision() { point = NULL; triangle = NULL; }
    void draw();
    void addImpulse(double impulse, Vector3d &norm, bool swap, std::string &print);
    void inelasticForce();
    void springForce(double dt, double h, double k);
    void geometricForce(double dt, double h, double k);
};

class EdgeEdgeCollision: public Collision {
public:
    Edge* edge1;
    Edge* edge2;
    double a, b;

    EdgeEdgeCollision(Edge *e1, Edge *e2, double a, double b): edge1(e1), edge2(e2), a(a), b(b) {}
    ~EdgeEdgeCollision() { edge1 = edge2 = NULL; }
    void draw();
    void addImpulse(double impulse, Vector3d &norm, bool swap, std::string &print);
    void inelasticForce();
    void springForce(double dt, double h, double k);
    void geometricForce(double dt, double h, double k);
};

class Force {
public:
    virtual ~Force() {}
    virtual void addForces(VectorXd &f) = 0;
    virtual void addJacobians(MatrixXd &Jx, MatrixXd &Jv) = 0;
    virtual void draw() = 0;
};

class Gravity: public Force {
public:
    ParticleSystem *ps;
    Vector3d g;

    Gravity(ParticleSystem *ps, Vector3d g): ps(ps), g(g) {}
    ~Gravity() { ps = 0; }
    void addForces(VectorXd &f);
    void addJacobians(MatrixXd &Jx, MatrixXd &Jv);
    void draw();
};

class DragForce: public Force {
public:
    ParticleSystem *ps; // apply drag to all particles
    double kd;          // drag coefficient

    DragForce(ParticleSystem *ps, double kd): ps(ps), kd(kd) {}
    ~DragForce() { ps = 0; }
    void addForces(VectorXd &f);
    void addJacobians(MatrixXd &Jx, MatrixXd &Jv);
    void draw();
};

class SpringForce: public Force {
    // connects two particles by a spring
public:
    Particle *p0, *p1; // particles
    double ks, kd;     // spring constant, damping coefficient
    double l0;         // rest length

    SpringForce(Particle *p0, Particle *p1, double ks, double kd, double l0):
        p0(p0), p1(p1), ks(ks), kd(kd), l0(l0) {}
    ~SpringForce() { p0 = p1 = 0; }
    void addForces(VectorXd &f);
    void addJacobians(MatrixXd &Jx, MatrixXd &Jv);
    void draw();
};

class BendForce: public Force {
public:
    Particle *p0, *p1, *p2;
    double k;

    BendForce(Particle *p0, Particle *p1, Particle *p2, double k):
        p0(p0), p1(p1), p2(p2) {}
    ~BendForce() { p0 = p1 = p2 = 0; }
    void addForces(VectorXd &f);
    void addJacobians(MatrixXd &Jx, MatrixXd &Jv);
    void draw();
};

class AnchorForce: public Force {
    // attaches a particle to a fixed point by a spring
public:
    Particle *p;   // particle
    Vector3d x;    // point to anchor it to
    double ks, kd; // spring constant, damping coefficient

    AnchorForce(Particle *p, Vector3d x, double ks, double kd):
        p(p), x(x), ks(ks), kd(kd) {}
    ~AnchorForce() { p = 0; }
    void addForces(VectorXd &f);
    void addJacobians(MatrixXd &Jx, MatrixXd &Jv);
    void draw();
};

#endif
