#include "integration.hpp"
#include <iostream>
#include <fstream>
#include <Eigen/Sparse>
#include <Eigen/IterativeLinearSolvers>
using namespace std;

void forwardEulerStep(PhysicalSystem *system, double dt) {
    int n = system->getDOFs();
    VectorXd x0(n), v0(n);
    system->getState(x0, v0);
    MatrixXd M(n,n);
    system->getInertia(M);
    VectorXd f0(n);
    system->getForces(f0);
    VectorXd a0(n); // acceleration
    for (int i = 0; i < n; i++)
        a0(i) = f0(i)/M(i,i);
    VectorXd x1 = x0 + v0*dt;
    VectorXd v1 = v0 + a0*dt;
    system->setState(x1, v1, dt);
}

void leapfrogStep(PhysicalSystem *system, double dt) {
    int n = system->getDOFs();
    VectorXd x0(n), v0(n);
    system->getState(x0, v0);
    MatrixXd M(n,n);
    system->getInertia(M);
    VectorXd f0(n);
    system->getForces(f0);
    VectorXd a0(n); // acceleration
    for (int i = 0; i < n; i++)
        a0(i) = f0(i)/M(i,i);
    VectorXd v1 = v0 + a0*dt;
    VectorXd x1 = x0 + v1*dt;
    system->setState(x1, v1, dt);
}

VectorXd solve(const MatrixXd &A, const VectorXd &b) {
    SparseMatrix<double> spA = A.sparseView();
    ConjugateGradient< SparseMatrix<double> > solver;
    return solver.compute(spA).solve(b);
}

void backwardEulerStep(PhysicalSystem *system, double dt) {
    // get states
    int n = system->getDOFs();
    // Get current particle states
    VectorXd x0(n), v0(n);
    system->getState(x0, v0);
    // Get Mass Matrix
    MatrixXd M(n,n);
    system->getInertia(M);
    // Get Forces
    VectorXd f0(n);
    system->getForces(f0);
    // Get Jacobians
    MatrixXd Jx(n,n), Jv(n,n);
    system->getJacobians(Jx, Jv);
    //Compute Matrix A = M - Jx*dt^2 - Jv*dt and Vector b = f0 + Jx*v0*dt
    MatrixXd A = M - Jx*(dt*dt) - Jv*dt;
    VectorXd b = f0 + Jx*v0*dt;

    //Solve A*dv = b for dv
    VectorXd dv = solve(A, b);

    //Compute New State
    VectorXd v1 = v0 + dv*dt;
    VectorXd x1 = x0 + v1*dt;

    system->setState(x1, v1, dt);
}

void midpointStep(PhysicalSystem *system, double dt) {
    int n = system->getDOFs();
    VectorXd x0(n), v0(n);
    system->getState(x0, v0);
    MatrixXd M(n,n);
    system->getInertia(M);
    VectorXd f0(n);
    system->getForces(f0);
    VectorXd a0(n); // acceleration
    for (int i = 0; i < n; i++)
        a0(i) = f0(i)/M(i,i);
    VectorXd vh = v0 + a0*dt/2.0;
    VectorXd x1 = x0 + vh*dt;
    VectorXd v1 = vh + a0*dt/2.0;
    system->setState(x1, v1, dt);
}
