CXX := g++

CXXFLAGS := -Wall -g -Wno-sign-compare -I/usr/include/eigen3 -I/opt/local/include/eigen3 -O3 `pkg-config --cflags opencv`
# -fopenmp -O3

UNAME := $(shell uname)
ifeq ($(UNAME), Darwin) # Mac
	LDFLAGS := -fopenmp -framework GLUT -framework OpenGL
else # Linux?
	LDFLAGS := -lglut -lGLU -lGL -fopenmp `pkg-config --libs opencv` -lgsl -lgslcblas
endif

OBJ := integration.o particles.o

all: run run2 run3

run: main.o $(OBJ)
	$(CXX) $^ -o $@ $(LDFLAGS)

run2: main2.o $(OBJ)
	$(CXX) $^ -o $@ $(LDFLAGS)

run3: main3.o $(OBJ)
	$(CXX) $^ -o $@ $(LDFLAGS)

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $< -o $@

# Nicked from http://www.gnu.org/software/make/manual/make.html#Automatic-Prerequisites
%.d: %.cpp
	@set -e; rm -f $@; \
	$(CXX) -MM $(CXXFLAGS) $< -o $@.tmp; \
	sed 's,\($*\)\.o[ :]*,\1.o: ,g' < $@.tmp > $@; \
	rm -f $@.tmp

-include $(OBJ:.o=.d)

clean:
	rm -f *.o *.d run run2 run3
