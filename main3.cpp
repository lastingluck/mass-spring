/*
* This main file produces 2 cloths that will hopefully interact together
*/

#include "particles.hpp"
#include "opengl.hpp"
#include <string>
#include <cv.h>
#include <highgui.h>

//#define EIGEN_DONT_PARALLELIZE 1

int w = 800, h = 800;                          // size of window in pixels
double xmin = 0, xmax = 1, ymin = 0, ymax = 1; // range of coordinates drawn
double dt = 1.0/60.0;                          // time step
Vector3d eye(1.15, -0.25, 3.0);				   // camera positon
Vector3d center(1.15, -0.25, 2.0);			   // point to look at
Vector3d up(0.0, 1.0, 0.0);					   // camera up vector
int prevx, prevy;
int mouseButton = 0;                           // 0-none, 1-left, 2-right, 3-left&shift, 4-right&shift
int substeps = 10;

bool dc = false;
bool flip = false;
bool hold = true;

extern const int m = 21;
extern const int n = 21;

//Recording stuff
cv::Mat img(h, w, CV_8UC3);
cv::Mat flipped(h, w, CV_8UC3);
cv::VideoWriter output;
int frameNum = 0, secondNum = 0;

double fov = 70, ar = 1, np = 0.001, fp = 100;
int lastTime = 0, prevTime = 0, frame = 0;

std::vector<ParticleSystem*> psys;
AnchorForce mouseForce(NULL, Vector3d::Zero(), 1000, 4);
std::vector<AnchorForce*> anchors;
int movePoints = 0; //1 - move closer, 2 - move away

void display() {
    glClearColor(1,1,1,1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(fov, ar, np, fp);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(eye(0), eye(1), eye(2), center(0), center(1), center(2), up(0), up(1), up(2));
    for(int i = 0; i < psys.size(); i++) {
        psys[i]->draw(flip);
    }

    //write backbuffer to opencv mat and video
    glReadPixels(0, 0, img.cols, img.rows, GL_BGR, GL_UNSIGNED_BYTE, img.data);
    cv::flip(img, flipped, 0);
    output << flipped;
    frameNum++;
    //printf("Frame %d\n", frameNum);
    if(frameNum == 30) {
        secondNum++;
        if(secondNum == 2) {
            movePoints = 1;
        }
        if(secondNum == 4) {
            std::exit(0);
        }
        //printf("Output Second %d\n", secondNum);
        frameNum = 0;
    }

    glutSwapBuffers();
}

void reshape(int w, int h) {
    ::w = w;
    ::h = h;
    glViewport(0, 0, w, h);
}

void idle() {
    //FPS
    frame++;
    int time = glutGet(GLUT_ELAPSED_TIME);
    if(time - lastTime > 1000) {
        double fps = frame * 1000.0 / (time-lastTime);
        lastTime = time;
        frame = 0;
        //printf("FPS: %f\n", fps);
        char fpsString[44];
        snprintf(fpsString, 44, "FPS:%f  Seconds:%d  Frames:%d", fps, secondNum, frameNum);
        glutSetWindowTitle(fpsString);
    }
    //Move anchor points
    if(movePoints == 1) {
        hold = false;
    }
    for (int i = 0; i < substeps; i++) {
        //double timeStep = (time-prevTime) / 1000.0;
        //psys.step(timeStep/substeps);
        if(hold) {
            std::vector<ParticleSystem*>::const_iterator first = psys.begin() + 1;
            std::vector<ParticleSystem*>::const_iterator last = psys.end();
            std::vector<ParticleSystem*> newVec(first, last);
            newVec[0]->step(::dt/substeps, newVec);
        }
        else {
            psys[0]->step(::dt/substeps, psys);
        }
    }
    prevTime = time;
    glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) {
    if (button == GLUT_LEFT_BUTTON) {
        if (state == GLUT_DOWN) {
            if(glutGetModifiers() == GLUT_ACTIVE_SHIFT) {
                mouseButton = 3;
            }
            else {
                mouseButton = 1;
            }
            GLdouble model[16];
            GLdouble proj[16];
            GLint view[4];
            glGetDoublev(GL_MODELVIEW_MATRIX, model);
            glGetDoublev(GL_PROJECTION_MATRIX, proj);
            glGetIntegerv(GL_VIEWPORT, view);
            double xpos1, ypos1, zpos1, xpos2, ypos2, zpos2;
            gluUnProject((double)x, view[3]-(double)y, 0, model, proj, view, &xpos1, &ypos1, &zpos1);
            gluUnProject((double)x, view[3]-(double)y, 1, model, proj, view, &xpos2, &ypos2, &zpos2);
            ::mouseForce.x = Vector3d(xpos1, ypos1, zpos1);

            // find nearest particle
            double dmin = 1e9;
            double maxd = 5.5;
            Vector3d rdir = Vector3d(xpos2, ypos2, zpos2) - Vector3d(xpos1, ypos1, zpos1);
            for (int i = 0; i < psys[0]->particles.size(); i++) {
                Particle *p = psys[0]->particles[i];
                double d = (rdir.cross(p->x - ::mouseForce.x)).norm();
                if (d < dmin && d < maxd) {
                    ::mouseForce.p = p;
                    dmin = d;
                }
            }
            if(::mouseForce.p != NULL) {
                Vector3d u = ::mouseForce.p->x - ::mouseForce.x;
                Vector3d intersect = ::mouseForce.x + (u.dot(rdir)/rdir.squaredNorm()) * rdir;
                ::mouseForce.x = intersect;
            }
            //printf("Intersect Position: %f, %f, %f\n", intersect(0), intersect(1), intersect(2));
            //printf("Particle Position: %f, %f, %f\n", ::mouseForce.p->x(0), ::mouseForce.p->x(1), ::mouseForce.p->x(2));
        } else if (state == GLUT_UP) {
            mouseButton = 0;
            ::mouseForce.p = NULL;
        }
    }
    else if(button == GLUT_RIGHT_BUTTON) {
        if(state == GLUT_DOWN && glutGetModifiers() == GLUT_ACTIVE_SHIFT) {
            mouseButton = 4;
            prevx = x;
            prevy = y;
        }
        else if(state == GLUT_UP) {
            mouseButton= 0;
        }
    }
}

void motion(int x, int y) {
    if(mouseButton == 1) { //left
        if(::mouseForce.p != NULL) {
            GLdouble model[16];
            GLdouble proj[16];
            GLint view[4];
            glGetDoublev(GL_MODELVIEW_MATRIX, model);
            glGetDoublev(GL_PROJECTION_MATRIX, proj);
            glGetIntegerv(GL_VIEWPORT, view);
            double xpos1, ypos1, zpos1, xpos2, ypos2, zpos2;
            gluUnProject((double)x, view[3]-(double)y, 0, model, proj, view, &xpos1, &ypos1, &zpos1);
            gluUnProject((double)x, view[3]-(double)y, 1, model, proj, view, &xpos2, &ypos2, &zpos2);
            ::mouseForce.x = Vector3d(xpos1, ypos1, zpos1);
            Vector3d rdir = Vector3d(xpos2, ypos2, zpos2) - Vector3d(xpos1, ypos1, zpos1);
            Vector3d u = ::mouseForce.p->x - ::mouseForce.x;
            Vector3d intersect = ::mouseForce.x + (u.dot(rdir)/rdir.squaredNorm()) * rdir;
            ::mouseForce.x = intersect;
        }
    }
    else if(mouseButton == 3) { //shift & left
        //rotate camera around center
    }
    else if(mouseButton == 4) { //shift & right
        int difx = x - prevx;
        int dify = -(y - prevy);
        double sensitivity = 0.005;
        Vector3d right = (center-eye).cross(up);
        eye += sensitivity * difx * right;
        center += sensitivity * difx * right;
        eye += sensitivity * dify * up;
        center += sensitivity * dify * up;
        prevx = x;
        prevy = y;
    }
}

void keyboard(unsigned char key, int x, int y) {
    switch(key) {
        case 'm':
            substeps = substeps + 1;
            printf("Substeps: %d\n", substeps);
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
            }
            psys[0]->forces.push_back(&mouseForce);
            break;
        case 'n':
            substeps = (substeps == 1) ? 1 : substeps-1;
            printf("Substeps: %d\n", substeps);
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
            }
            psys[0]->forces.push_back(&mouseForce);
            break;
        case '1':
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
                psys[i]->integrator = FORWARD;
            }
            psys[0]->forces.push_back(&mouseForce);
            printf("Forward Euler\n");
            substeps = 100;
            printf("Substeps: %d\n", substeps);
            break;
        case '2':
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
                psys[i]->integrator = BACKWARD;
            }
            psys[0]->forces.push_back(&mouseForce);
            printf("Backward Euler\n");
            substeps = 1;
            printf("Substeps: %d\n", substeps);
            break;
        case '3':
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
                psys[i]->integrator = LEAPFROG;
            }
            psys[0]->forces.push_back(&mouseForce);
            printf("Leapfrog\n");
            substeps = 90;
            printf("Substeps: %d\n", substeps);
            break;
        case '4':
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
                psys[i]->integrator = MIDPOINT;
            }
            psys[0]->forces.push_back(&mouseForce);
            printf("Midpoint\n");
            substeps = 70;
            printf("Substeps: %d\n", substeps);
            break;
        case '5':
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->init();
                psys[i]->integrator = FORWARD;
            }
            psys[0]->forces.push_back(&mouseForce);
            printf("Midpoint\n");
            substeps = 200;
            printf("Substeps: %d\n", substeps);
            break;
        case 27: //escape
            exit(0);
            break;
        case 'w':
            for(int i = 0; i < psys.size(); i++) {
                psys[i]->wireframe = !psys[i]->wireframe;
            }
            break;
        case 'c':
            dc = !dc;
            break;
    }
}

int main(int argc, char **argv) {
    //Eigen::initParallel();
    psys.push_back(new ParticleSystem());
    psys.push_back(new ParticleSystem(0.02, 10, 1.2, 2.3, 0.0, 1.1));
    for(int i = 0; i < psys.size(); i++) {
        psys[i]->init();
    }
    psys[0]->forces.push_back(&mouseForce);
    anchors[0]->p = NULL;
    anchors[1]->p = NULL;
    for(int i = 0; i < psys[0]->particles.size(); i++) {
        psys[0]->particles[i]->v += Vector3d(7, 4, 0);
    }
    //anchors[0]->p = psys[0]->particles[psys[0]->particles.size()-m];
    //anchors[0]->x = anchors[0]->p->x;
    //anchors[1]->p = psys[0]->particles[m-1];
    //anchors[1]->x = anchors[1]->p->x;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_MULTISAMPLE|GLUT_DEPTH);
    glutInitWindowSize(::w, ::h);
    glutCreateWindow("FPS:0.0  Seconds:0  Frames:0");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(keyboard);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
    //glEnable(GL_CULL_FACE);

    //glEnable(GL_COLOR_MATERIAL);
    //glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);
    //glColorMaterial(GL_FRONT_AND_BACK, GL_SPECULAR);
    float diffFront[] = {0.0f, 0.0f, 1.0f, 1.0f};
    float specFront[] = {0.6f, 0.6f, 0.6f, 1.0f};
    float diffBack[] = {0.0f, 1.0f, 0.0f, 1.0f};
    float specBack[] = {0.6f, 0.6f, 0.6f, 1.0f};
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diffFront);
    glMaterialfv(GL_FRONT, GL_SPECULAR, specFront);
    glMaterialfv(GL_BACK, GL_DIFFUSE, diffBack);
    glMaterialfv(GL_BACK, GL_SPECULAR, specBack);
    glEnable(GL_NORMALIZE);
    GLfloat ac[] = {0.4, 0.4, 0.4, 1.0};
    GLfloat dc[] = {0.3, 0.3, 0.3, 1.0};
    GLfloat sc[] = {0.7, 0.7, 0.7, 1.0};
    GLfloat lp[] = {-1.0, -1.0, -1.0, 0};
    GLfloat lp2[] = {1.0, 1.0, 1.0, 0};
    glLightfv(GL_LIGHT0, GL_AMBIENT, ac);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, dc);
    glLightfv(GL_LIGHT0, GL_SPECULAR, sc);
    glLightfv(GL_LIGHT0, GL_POSITION, lp);
    glLightfv(GL_LIGHT1, GL_AMBIENT, ac);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, dc);
    glLightfv(GL_LIGHT1, GL_SPECULAR, sc);
    glLightfv(GL_LIGHT1, GL_POSITION, lp2);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);

    //use fast 4-byte alignment (default anyway) if possible
    glPixelStorei(GL_PACK_ALIGNMENT, (img.step & 3) ? 1 : 4);
    //set length of one complete row in destination data (doesn't need to equal img.cols)
    glPixelStorei(GL_PACK_ROW_LENGTH, img.step/img.elemSize());
    output = cv::VideoWriter("test.avi", CV_FOURCC('P', 'I', 'M', '1'), 30, cv::Size(w, h));
    //output = cv::VideoWriter("test.avi", -1, 30, cv::Size(h, w));

    glutMainLoop();
}
